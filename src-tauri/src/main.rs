#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

use std::process::{Child, Stdio};
use std::thread::sleep;
use std::time::Duration;

use command_group::{CommandGroup, GroupChild};
use std::process::Command as StdCommand;
use tauri::api::process::{Command, CommandChild};
use tauri::RunEvent;

#[derive(Default)]
struct Backend(Option<GroupChild>);

fn main() {
    println!("Started App");
    let mut backend = Backend::default();

    tauri::Builder::default()
        .build(tauri::generate_context!())
        .expect("Error building app")
        .run(move |app_handle, event| match event {
            RunEvent::Ready => {
                println!("Starting backend...");

                let mut command = StdCommand::from(
                    Command::new_sidecar("backend_server")
                        .expect("Failed to create `backend_server` binary command"),
                );

                /*
                NOTE: https://github.com/tauri-apps/tauri/issues/4949
                The pyinstaller has a bootloader process which starts the actual python process,
                this is the PID we see here.
                If we just send a SIGKILL to the bootloader process, it dies orphaning the backend process.
                This library https://github.com/watchexec/command-group creates a group,
                and sends SIGKILL to all the processes in the group.
                TODO: Check if this works on Windows/Mac.
                */

                let child = command
                    .group_spawn()
                    .expect("Failed to spawn backend sidecar");
                println!("Child PID: {}", child.id().to_string());

                _ = backend.0.insert(child);
                println!("Backend started.");
            }
            RunEvent::ExitRequested { .. } => {
                println!("Shutting down backend...");
                if let Some(mut child) = backend.0.take() {
                    child.kill().expect("Failed to shutdown backend.");
                    child.wait().expect("Failed to wait for child to die.");
                    println!("Backend gracefully shutdown.")
                }
            }
            _ => {}
        });
}

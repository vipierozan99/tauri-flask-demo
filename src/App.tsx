import { useState } from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";

import { fetch, ResponseType } from "@tauri-apps/api/http";

function FetchBtn() {
  const [response, setResponse] = useState("Fetch!");

  const getResponse = async () => {
    const resp = (
      await fetch("http://localhost:5000/hello", {
        method: "GET",
        responseType: ResponseType.Text,
      })
    ).data as string;
    console.log(resp);
    setResponse(resp);
  };

  return <button onClick={getResponse}>{response}</button>;
}

function App() {
  const [count, setCount] = useState(0);

  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.tsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
      <FetchBtn />
    </div>
  );
}

export default App;
